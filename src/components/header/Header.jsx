import React from 'react';
import logo from './logo.svg';
import './Header.css';

//ecriture du composant Header sous objet
export class Header extends React.Component {
    render() {
    return (
      <div className="Header">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
         
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            {this.props.name}
          </a>
        </header>
      </div>
    );
  }
}
  
 