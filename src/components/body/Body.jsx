import React from 'react';
import { Welcome } from '../welcome/Welcome';

class Body extends React.Component {   
    constructor(props){
      super(props);
      this.handlePauseChange = this.handlePauseChange.bind(this);
      this.setDateChange = this.setDateChange.bind(this);

      this.state = {
        onPause : false,
        dateTab : [new Date(), new Date(), new Date()],
      };

    }

    //methodes
    handlePauseChange(event){
      this.setState({onPause : event});
    }

    setDateChange(date, targetName){      
      if(targetName === "inputForm") {
        this.setState({        
          dateTab : [date, this.state.dateTab[0], this.state.dateTab[2]]
        });
      }else if(targetName === "selectForm"){
        this.setState({ 
          dateTab : [this.state.dateTab[1], date, this.state.dateTab[2]]
        });
      }
    
    }

    componentDidMount() {      
      if(!this.state.onPause){
        this.timerID = setInterval(
        () => this.tick(),
        1000
      );
      }else{
        clearInterval(this.timerID);
      }
      
    }

    componentWillUnmount() {
      if(this.state.onPause) {
        clearInterval(this.timerID);
      }
    }

    tick() {
      //usage de la methode setState pour mettre à jour le state      
      if(!this.state.onPause) {
        this.setState({
          //increment current dates by 1 second        
          dateTab : [new Date(this.state.dateTab[0].getTime() + 1000), new Date(this.state.dateTab[1].getTime() + 1000), new Date(this.state.dateTab[2].getTime() + 1000)]
        });
      }
  }


    render() {
    return (
      <div className="Body">        
        <Welcome setDate={this.setDateChange} onPauseChange={this.handlePauseChange} id="1" onPause={this.state.onPause} dateValue={this.state.dateTab[0]} />
        <Welcome setDate={this.setDateChange} onPauseChange={this.handlePauseChange} id="2" onPause={this.state.onPause} dateValue={this.state.dateTab[1]} />
        <Welcome setDate={this.setDateChange} onPauseChange={this.handlePauseChange} id="3" onPause={this.state.onPause} dateValue={this.state.dateTab[2]} />
      </div>
    );
  }
}
  
export default Body;

