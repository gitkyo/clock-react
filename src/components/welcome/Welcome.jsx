import React from 'react';
import { WarningBanner } from '../WarningBanner/WarningBanner';
import './Welcome.css'

export class Welcome extends React.Component {
    //constructeur
    constructor(props) {
        //super pour récupérer les props du parent
        super(props);

        //surcharge avec un state
        this.state = { 
          // onPause : false, 
          date: new Date(), 
          valueInputTxt: '',
          valueSelectOption : 'option1',
          valueCheckBoxOption : 'checkOption1',
          errorApi :  false,
          listCity : [],
          timezone : 'La Réunion'
        };

        //initalisation facultative de la variable pour le timer
        // this.timerID = null;

        // Cette liaison est nécéssaire afin de permettre
        // l'utilisation de `this` dans la fonction de rappel.
        this.eventPauseBtn = this.eventPauseBtn.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    //methode particulire lors de la creation du composant
    componentDidMount() {
        // if(!this.props.onPause) {
          // this.timerID = setInterval(
          //   () => this.tick(),
          //   1000
          // );
        // }
        

        //get list of city from api
        this.getListCity();
    }
    
    //methode particulire lors de la destruction du composant
    componentWillUnmount() {
      // if(this.props.onPause) {
        // clearInterval(this.timerID);
      // }
    }

    //methode
    // tick() {
    //     //usage de la methode setState pour mettre à jour le state
    //     this.setState({
    //       //increment current date by 1 second
    //       date : new Date(this.state.date.getTime() + 1000)
    //     });
    // }

    // methode get list of city from api
    getListCity(){
      fetch('https://worldtimeapi.org/api/timezone/Europe/')
      .then(response => response.json())
      .then(data => {          
        
        if(!data.error){          

          let listCity = data.map((city, index) => 
            <option key={index} value={city.split("Europe/")[1]}>{city.split("Europe/")[1]}</option>            
          );

          this.setState({
            listCity : listCity
          });
        }
      });
    }

    handleSubmit(event) {   
      //casser le comportement par defaut du formulaire   
      event.preventDefault();     
      let targetName = event.target.name;
      let value = "";
      
      if(targetName === "inputForm"){
        value = this.state.valueInputTxt;
      }else if(targetName === "selectForm"){
        value = this.state.valueSelectOption;
      }else if(targetName === "checkBoxForm"){

      }      
      
      if(value.length > 0){

        
        fetch('https://worldtimeapi.org/api/timezone/Europe/'+value)
        .then(response => response.json())
        .then(data => {
          // on créer une date à partir de la date récupérée depuis l'API qui est converti dans le bon format
          // console.log(data);
          if(!data.error){
            let date  = new Date(data.datetime.split('.')[0].toString());
            
            // on met à jour le state avec la nouvel date
            this.props.setDate(date, targetName);
            this.setState({
              // date: date,
              errorApi : false,
              timezone : data.timezone
            });
          }else{
            this.setState({
              errorApi : true
            });
          }
        
        })
           
        
      }
    }

    handleChange (event) {  
      let name = event.target.name;    
      let value = event.target.value;      
        
      if(name === "inputText") {
        this.setState({
            valueInputTxt: value
        });
      }else if(name === "select"){      
        this.setState({
            valueSelectOption: value
        });
      }else if(name === "checkOption1"){
        this.setState({
            valueCheckBoxOption: value
        });
      }
    }

    //methode pause event
    eventPauseBtn(){
              
        if(!this.props.onPause) {
          
          // appell de unmount pour activer le composant
          this.componentWillUnmount();         

          //mise a jour du state du parent via une méthode envoyé par Body via les props
          this.props.onPauseChange(true);

        }
        else{ 
          // appel de unmount pour désactiver le composant
          this.componentDidMount();

          //mise a jour du state du parent via une méthode envoyé par Body via les props
          this.props.onPauseChange(false);
        }

    }

    //methode render de react qui renvoie un JSX appele a chaque changement d'état
    render() {  
      
    
      
      let form = "";
      if(this.props.id === "1") {      
        form = 
        <form name="inputForm" onSubmit={this.handleSubmit}>
          <h3>Entrez un nom de ville en Europe (cf liste déroulante plus bas) : </h3>
          <label htmlFor="text">Fuseau horraire ? (ex : London, Moscow) </label>            
          <input name="inputText" type="text" value={this.state.valueInputTxt} onChange={this.handleChange} />
          <input type="submit" value="Envoyer" />
        </form>
      }else if(this.props.id === "2") {
        form = 
        <form name="selectForm" onSubmit={this.handleSubmit}>
          <h3>Sélectionner une ville d'Europe : </h3>
          <label htmlFor="text">Label </label>            
          <select name="select" value={this.state.valueSelectOption} onChange={this.handleChange}>
            {this.state.listCity}
          </select>
          <input type="submit" value="Envoyer" />
        </form>
      }else if(this.props.id === "3") {
        form = 
        <form name="checkBoxForm" onSubmit={this.handleSubmit}>
          <h3>Exemple non fonctionnel du checkbox : </h3>
          <label htmlFor="text">Label </label>            
          <input name="inputCheckBox" type="checkbox" value={this.state.valueCheckBoxOption} onChange={this.handleChange} />
          <input type="submit" value="Envoyer" />
        </form>
      }

      return (
        <div className="Welcome">
            { this.props.onPause && !this.state.error ? <WarningBanner message="Horloge en pause"/> : null } 
            { this.state.errorApi ? <WarningBanner message="Veuillez entrer un nom de ville valide"/> : null } 
            <h1>Bonjour, {this.state.timezone}</h1>
            <h2>Il est {this.props.dateValue.toLocaleTimeString()}.</h2>     
            <button onClick={this.eventPauseBtn}>{this.props.onPause ? "Start" : "Pause"}</button>
            <br/>
            {form}
            
            
            {/* 
            todo : 
            - synchroniser l'état de pause
            - api heures : https://worldtimeapi.org/api/timezone/Europe/London
             
            - mettre sur gitlab et netlify/heroku
            */}

            {/* <Img stateW={this.state} /> */}
        </div>
      );
    }
  }
 
