import { Header } from '../header/Header'
import Body  from '../body/Body'
import './App.css';

//ecriture du composant app sous forme fonction
function App() {
  return (
    <div className="App">
      <Header name="World Clock App - A Simple Learning project"/>
      <Body />
    </div>
  );
}

export default App;
